from django.urls import path
from .views import (
    movies,
    top
)

app_name = 'movies'

urlpatterns = [
    path('movies', movies, name="movies"),
    path('top', top, name="top"),
]
