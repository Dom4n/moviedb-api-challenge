import pytest
from django.urls import reverse

pytestmark = pytest.mark.django_db


def test_movie():
    url = reverse('movies:movies')
    assert url == '/movies'


def test_top():
    url = reverse('movies:top')
    assert url == '/top'
