import pytest
from django.db import IntegrityError

from movies.models import Movie


@pytest.mark.django_db
def test_title_should_be_unique():
    movie = Movie.objects.create(
        title='Terminator',
        details={}
    )

    assert movie.title == 'Terminator'

    with pytest.raises(IntegrityError):
        movie2 = Movie.objects.create(
            title='Terminator',
            details={}
        )

