import datetime

import pytest
from django.urls import reverse
from freezegun import freeze_time

from comments.models import Comment
from comments.tests.factory import CommentFactory
from movies.models import Movie
from movies.tests.factory import MovieFactory
from utils import omdb_api

pytestmark = pytest.mark.django_db

movies_url = reverse('movies:movies')
top_url = reverse('movies:top')


def test_movies_get_works(client):
    response = client.get(movies_url)
    assert response.status_code == 200


def test_movies_get_returns_json_list(client):
    response = client.get(movies_url)
    assert response.json() == []

    Movie.objects.bulk_create([
        Movie(title="Movie1", details={}),
        Movie(title="Movie2", details={}),
        Movie(title="Movie3", details={}),
    ])

    response = client.get(movies_url)
    assert len(response.json()) == 3


DETAILS = {"Title": "Movie1"}


@pytest.fixture
def get_movie_details_mocked(monkeypatch):
    def mocked_details(*args, **kwargs):
        return DETAILS

    monkeypatch.setattr(omdb_api, "get_movie_details", mocked_details)


def test_movies_post_must_pass_title(client):
    response = client.generic("POST", movies_url, "")
    assert response.status_code == 409
    assert "error" in response.json()
    assert Movie.objects.count() == 0


def test_movies_post_omdb_general_error(client, monkeypatch):
    def mocked_details(*args, **kwargs):
        return None

    monkeypatch.setattr(omdb_api, "get_movie_details", mocked_details)

    response = client.generic("POST", movies_url, "Movie1")
    assert response.status_code == 503
    assert "error" in response.json()


def test_movies_post_omdb_movie_not_found(client, monkeypatch):
    def mocked_details(*args, **kwargs):
        return {"Error": "Something went wrong"}

    monkeypatch.setattr(omdb_api, "get_movie_details", mocked_details)
    response = client.generic("POST", movies_url, "Movie1")
    assert response.status_code == 404
    assert "error" in response.json()


def test_movies_post_adds_movie(client, get_movie_details_mocked):
    response = client.generic("POST", movies_url, "Movie1")

    assert response.status_code == 200
    assert Movie.objects.count() == 1
    assert response.json() == {"id": Movie.objects.first().pk, **Movie.objects.first().details}


def test_movies_post_cant_add_duplicate_movie(client, get_movie_details_mocked):
    response = client.generic("POST", movies_url, "Movie1")
    assert response.status_code == 200
    assert Movie.objects.count() == 1

    response2 = client.generic("POST", movies_url, "Movie1")
    assert response2.status_code == 409
    assert "error" in response2.json()
    assert Movie.objects.count() == 1


def test_top_endpoint_basic(client):
    movie1 = MovieFactory()
    movie2 = MovieFactory()
    movie3 = MovieFactory()
    movie4 = MovieFactory()

    CommentFactory(movie=movie1)
    CommentFactory(movie=movie2)
    CommentFactory(movie=movie2)
    CommentFactory(movie=movie3)
    CommentFactory(movie=movie3)
    CommentFactory(movie=movie3)
    CommentFactory(movie=movie4)

    response = client.get(top_url)
    assert response.status_code == 200
    assert response.json() == [
        {
            "movie_id":       movie3.pk,
            "total_comments": movie3.comments.count(),
            "rank":           1
        },
        {
            "movie_id":       movie2.pk,
            "total_comments": movie2.comments.count(),
            "rank":           2
        },
        {
            "movie_id":       movie1.pk,
            "total_comments": movie1.comments.count(),
            "rank":           3
        },
        {
            "movie_id":       movie4.pk,
            "total_comments": movie4.comments.count(),
            "rank":           3
        },
    ]


def test_top_endpoint_datetime_filter(client):
    movie1 = MovieFactory()
    movie2 = MovieFactory()

    # old comments
    with freeze_time("2012-01-14 12:00:00"):
        CommentFactory(movie=movie1)
        CommentFactory(movie=movie1)

    # new comments
    CommentFactory(movie=movie2)
    CommentFactory(movie=movie2)
    CommentFactory(movie=movie2)

    assert Comment.objects.count() == 5

    all_time_top_movies = client.get(top_url)
    assert len(all_time_top_movies.json()) == 2

    only_old_top = client.get(top_url, {"end": datetime.datetime(2012, 1, 14, 13, 0)})
    assert len(only_old_top.json()) == 1

    only_new_top = client.get(top_url, {"start": datetime.datetime.utcnow() - datetime.timedelta(hours=12)})
    assert len(only_new_top.json()) == 1

    all_time_top_with_timerange = client.get(top_url, {
        "start": datetime.datetime(2000, 1, 1, 0, 0),
        "end":   datetime.datetime.utcnow() + datetime.timedelta(hours=12)
    })
    assert len(all_time_top_with_timerange.json()) == 2
