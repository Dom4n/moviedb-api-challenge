from django.urls import path
from .views import (
    comments
)

app_name = 'comments'

urlpatterns = [
    path('', comments, name="comments")
]
