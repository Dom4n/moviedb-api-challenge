from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from comments.models import Comment


@csrf_exempt
def comments(request):
    if request.method == "POST":
        comment = Comment.objects.create(
            movie_id=request.POST.get("movie"),
            comment=request.POST.get("comment")
        )
        return JsonResponse({"comment": comment.comment})

    comments_qs = Comment.objects.all().select_related('movie')
    if "movie" in request.GET:
        comments_qs = comments_qs.filter(movie_id=request.GET.get("movie"))
    response = [
        {
            "comment": c.comment,
            "created_at": c.created_at,
            "movie_id": c.movie_id
        } for c in comments_qs
    ]
    return JsonResponse(response, safe=False)
