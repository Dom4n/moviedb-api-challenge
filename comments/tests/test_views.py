import factory
import pytest
from django.urls import reverse

from comments.tests.factory import CommentFactory
from movies.tests.factory import MovieFactory

pytestmark = pytest.mark.django_db

url = reverse('comments:comments')


def test_get_all_comments(client):
    movie1 = MovieFactory()
    movie2 = MovieFactory()

    CommentFactory(movie=movie1)
    CommentFactory(movie=movie2)
    CommentFactory(movie=movie2)

    response = client.get(url)
    assert response.status_code == 200
    assert len(response.json()) == 3


def test_filter_comments_by_movie_id(client):
    movie1 = MovieFactory()
    movie2 = MovieFactory()

    CommentFactory(movie=movie1)
    CommentFactory(movie=movie2)
    CommentFactory(movie=movie2)

    response = client.get(url, {'movie': movie1.pk})
    assert len(response.json()) == 1

    response = client.get(url, {'movie': movie2.pk})
    assert len(response.json()) == 2


def test_post_new_comment(client):
    movie = MovieFactory()
    comment_text = factory.Faker('sentence', nb_words=40, variable_nb_words=True).generate()
    response = client.post(
        url,
        {'movie': movie.pk, 'comment': comment_text}
    )

    assert response.json()['comment'] == comment_text
