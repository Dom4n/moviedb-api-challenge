from django.urls import reverse


def test_comments():
    url = reverse('comments:comments')
    assert url == '/comments'
