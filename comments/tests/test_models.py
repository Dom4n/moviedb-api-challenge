import datetime

import pytest
import pytz
from freezegun import freeze_time

from comments.tests.factory import CommentFactory
from movies.tests.factory import MovieFactory


@pytest.mark.django_db
@freeze_time("2019-09-01 12:00:00")
def test_comment_model():
    movie = MovieFactory(title='Movie1')
    comment = CommentFactory(movie=movie, comment='Test comment.')

    assert comment.comment == 'Test comment.'
    assert comment.movie == movie
    assert comment.movie.title == "Movie1"
    assert comment.created_at == datetime.datetime(2019, 9, 1, 12, 0, 0, tzinfo=pytz.timezone('UTC'))

