import factory

from comments.models import Comment
from movies.tests.factory import MovieFactory


class CommentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Comment

    movie = factory.SubFactory(MovieFactory)
    comment = factory.Faker('sentence', nb_words=20, variable_nb_words=True)
