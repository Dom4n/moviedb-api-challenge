from utils.omdb_api import get_movie_details

import responses
from requests.exceptions import Timeout


@responses.activate
def test_response_status_not_200():
    responses.add(responses.GET, 'http://www.omdbapi.com/',
                  status=404)

    assert get_movie_details('') is None


@responses.activate
def test_omdb_timeout():
    responses.add(responses.GET, 'http://www.omdbapi.com/',
                  body=Timeout())

    assert get_movie_details('') is None


@responses.activate
def test_omdb_title_not_found():
    omdb_not_found_json_response = {'Error': 'Something went wrong.', 'Response': 'False'}
    responses.add(responses.GET, 'http://www.omdbapi.com/',
                  json=omdb_not_found_json_response,
                  status=200)

    assert get_movie_details('') == omdb_not_found_json_response


@responses.activate
def test_omdb_invalid_reponse():
    omdb_invalid_json_response = ''
    responses.add(responses.GET, 'http://www.omdbapi.com/',
                  body=omdb_invalid_json_response,
                  status=200)

    assert get_movie_details('') is None


@responses.activate
def test_omdb_return_valid_data():
    omdb_valid_json_response = {'Actors':     'Loris Basso, James Callahan, Debbie Medows, Michelle Kovach',
                                'Awards':     'N/A',
                                'BoxOffice':  'N/A',
                                'Country':    'USA',
                                'DVD':        'N/A',
                                'Director':   'Ben Hernandez',
                                'Genre':      'Short, Action, Sci-Fi',
                                'Language':   'English',
                                'Metascore':  'N/A',
                                'Plot':       'A cyborg comes from the future, to kill a girl named Sarah Lee.',
                                'Poster':     'N/A',
                                'Production': 'N/A',
                                'Rated':      'N/A',
                                'Ratings':    [{'Source': 'Internet Movie Database', 'Value': '6.2/10'}],
                                'Released':   'N/A',
                                'Response':   'True',
                                'Runtime':    '39 min',
                                'Title':      'Terminator',
                                'Type':       'movie',
                                'Website':    'N/A',
                                'Writer':     'James Cameron (characters), James Cameron (concept), Ben Hernandez '
                                              '(screenplay)',
                                'Year':       '1991',
                                'imdbID':     'tt5817168',
                                'imdbRating': '6.2',
                                'imdbVotes':  '23'}

    responses.add(responses.GET, 'http://www.omdbapi.com/',
                  json=omdb_valid_json_response,
                  status=200)

    assert get_movie_details('Terminator') == omdb_valid_json_response
